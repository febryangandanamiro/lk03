abstract class Kalkulator {
    double operan1, operan2;
    //Do your magic here...
    public void setOperan(double operan1, double operan2) {
        this.operan1 = operan1;
        this.operan2 = operan2;
    }
    public abstract double hitung();
}
